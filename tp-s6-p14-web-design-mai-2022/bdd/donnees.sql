
insert into admin(nomadmin, mdp) values('admin@gmail.com', 'admin');

insert into contenu(titre, idauteur, description, datePublication, dateCreation)
    values('Performance de l IA', default, 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Expedita reprehenderit accusamus, omnis obcaecati at facilis dignissimos consectetur dolorem eligendi deleniti eius quam labore, autem sit saepe in, corrupti sunt animi.', '2023-05-03 12:00:00', default);

insert into contenu(titre, idauteur, description, datePublication, dateCreation)
values('Roles de l IA', default, 'Expedita reprehenderit accusamus, omnis obcaecati at facilis dignissimos consectetur dolorem eligendi deleniti eius quam labore, autem sit saepe in, corrupti sunt animi.', '2023-05-03 16:05:00', default);

insert into contenu(titre, idauteur, description, datePublication, dateCreation)
values('Inconvenients de l IA', default, 'Expedita reprehenderit accusamus, omnis obcaecati at facilis dignissimos consectetur dolorem eligendi deleniti eius quam labore, autem sit saepe in, corrupti sunt animi.', '2023-05-04 08:14:00', default);

insert into categorie(nomcategorie) values ('Business');
insert into categorie(nomcategorie) values ('Nouveaute');